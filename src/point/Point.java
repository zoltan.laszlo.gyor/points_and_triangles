package point;

public class Point{ // f�jl neve == class nev�vel
  private int x;//v�ltoz�, de sz�m t�pus�
  private int y;

  public Point(int x, int y){ // konstruktor == class neve
    this.x = x;
    this.y = y;
  }
  public Point(int x){
    this.x = x;
    this.y = 2*x;
  }
  public Point(){
    this.x = 0;
    this.y = 0;
  }

  public void movePoint(int x, int y){
    this.x += x;
    this.y += y;
  }

  public int getX(){ //priv�tot el�rje minden olyan mint ha publikuss� tenn�m;
    return this.x;
  }
  public int getY(){
    return this.y;
  }
}
