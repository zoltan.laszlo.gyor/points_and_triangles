package triangle;

import point.Point;
import java.lang.Math;

public class Triangle{ //p�ld�nyas�tasz objektumot
  private Point a;//adattag, mert egy oszt�ly r�sz�t k�pzik
  private Point b;
  private Point c;

  public Triangle(){ //default konstruktor

  }

  public Triangle(Point a, Point b, Point c){
    this.a = a;
    this.b = b;
    this.c = c;
  }

  public Triangle(Point a, Point b){
    this.a = a;
    this.b = b;
    this.c = new Point(0, 0);
  }
  public Point getA(){
    return this.a;
  }
  public Point getB(){
    return this.b;
  }
  public Point getC(){
    return this.c;
  }
  public String toString(){ //sz�veg v�ltoz�
    String textTriangleA = "a point = " + this.a.getX() + " ; " + this.a.getY();
    String textTriangleB = "b point = " + this.b.getX() + " ; " + this.b.getY();
    String textTriangleC = "c point = " + this.c.getX() + " ; " + this.c.getY();
    return textTriangleA + " " + textTriangleB + " " + textTriangleC + " ";
  }
  public String calculateTriangleLength1(){ //double-t haszn�lj ha  a sz�mod nem eg�sz
    double d = Math.pow(this.a.getX() - this.b.getX(),2) + Math.pow(this.a.getX() - this.b.getY(),2);
    d = Math.sqrt(d); //gy�kvon�s
    String sides = "Az 1. oldal hossza: " + d;
    return sides;
  }
  public String calculateTriangleLength2(){
    double d = Math.pow(this.a.getX() - this.c.getX(),2) + Math.pow(this.a.getX() - this.c.getY(),2);
    d = Math.sqrt(d);
    String sides = "A 2. oldal hossza: " + d;
    return sides;
  }
  public String calculateTriangleLength3(){
    double d = Math.pow(this.b.getX() - this.c.getX(),2) + Math.pow(this.b.getX() - this.c.getY(),2);
    d = Math.sqrt(d);
    String sides = "A 3. oldal hossza: " + d;
    return sides;
  }
}