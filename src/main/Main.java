package main;

import point.Point;
import triangle.Triangle;

public class Main {
	public static void main(String[] args) {
		Point firstPoint = new Point(2,3);
	    Point secondPoint = new Point(2);
	    Point thirdPoint = new Point();

	    firstPoint.movePoint(3, 2);

	    System.out.println(firstPoint.getX() + " " + firstPoint.getY());
	    System.out.println(secondPoint.getX() + " " + secondPoint.getY());
	    System.out.println(thirdPoint.getX() + " " + thirdPoint.getY());

	    Point a = new Point(5,10);
	    Point b = new Point(3,7);
	    Point c = new Point(0,2);
	    Triangle firstTriangle = new Triangle(a,b,c);
	    Triangle secondTriangle = new Triangle(a,b);
	    System.out.println(firstTriangle.toString());
	    System.out.println(secondTriangle.toString());

	    System.out.println(firstTriangle.calculateTriangleLength1());
	    System.out.println(firstTriangle.calculateTriangleLength2());
	    System.out.println(firstTriangle.calculateTriangleLength3());

	    int[] tomb = new int[10];//t�mb�k, int=sz�mokhoz; majd meg kell szabni, hogy milyen hossz� a t�mb
	    char[] charTomb = new char[10];//t�mb->bet�h�z
	    tomb[0]  = 1;
	    System.out.println(tomb[0]);

	    for (int i=0; i<tomb.length; i++){
	      tomb[i] = i;
	      System.out.println(tomb[i]);
	    }
	}
}
